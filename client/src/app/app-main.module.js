import angular from 'angular';
import { AppModule } from './containers/app/app.module';

export const AppMainModule = angular
  .module('main', [AppModule])
  .name;

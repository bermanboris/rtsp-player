import JSMpeg from '../../../lib/jsmpeg.min';

export const AppComponent = {
  controllerAs: 'vm',
  template: `
    <div class="app col-12 col-xl-10 mx-auto">
      <h1 class="display-3">RTSP WebPlayer</h1>
      <div class="input-group mb-3">
        <input class="form-control" type="text" ng-model="vm.state.url" placeholder="RTSP Stream URL" />
        <div class="input-group-append">
          <button class="btn btn-primary" ng-click="vm.loadStream()">Load Stream</button>
        </div>
      </div>

      <canvas ng-click="vm.pauseStream()" class="col-12" id="video-canvas"></canvas>
    </div>
  `,
  controller: class AppComponent {
    constructor() {
      'ngInject';

      const wsUrl = 'ws://localhost:8080';
      const canvas = document.getElementById('video-canvas');

      this.state = {
        isPlaying: false,
        isLoading: false,
        url: 'rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov'
      };

      this.player = new JSMpeg.Player(wsUrl, { canvas }, {
        audio: true,
        preserveDrawingBuffer: true,
      });
    }


    loadStream() {
      this.player.source.socket.send(JSON.stringify({ type: 'OPEN_STREAM', url: this.state.url }));
      this.state.isPlaying = true;
      this.state.isLoading = true;
    }

    pauseStream() {
      if (this.state.isPlaying) {
        this.player.pause();
      } else {
        this.player.play();
      }

      this.state.isPlaying = !this.state.isPlaying;
    }
  }
};

import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import { AppComponent } from './app.component';

export const AppModule = angular
  .module('app', [uiRouter])
  .component('app', AppComponent)
  .config(($stateProvider, $locationProvider, $urlRouterProvider) => {
    'ngInject';

    $stateProvider.state('app', { url: '/', component: 'app' });
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
  })
  .name;

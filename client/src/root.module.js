import angular from 'angular';
import 'bootstrap/scss/bootstrap.scss';

import { RootComponent } from './root.component';
import { AppMainModule } from './app/app-main.module';
import './root.component.scss';

angular
  .module('root', [AppMainModule])
  .component('root', RootComponent)
  .value('EventEmmiter', payload => ({ $event: payload }))
  .name;

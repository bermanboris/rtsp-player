import * as WS from 'ws';
import { FFMpeg } from '../lib/ffmpeg';

export interface WebSocket extends WS.Server {
  broadcast?(data: any): void
  connectionCount?: number;
}

export interface WSInitParameters {
  onMessage(client: WSClient, message: object): void;
}

export interface WSClient extends WS {
  id: string;
  ffmpeg: FFMpeg;
}

export enum MessageTypes {
  OPEN_STREAM = "OPEN_STREAM"
}

export type OpenStreamMessage = {
  type: MessageTypes;
  url: string;
}
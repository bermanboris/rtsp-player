import { spawn, ChildProcess } from 'child_process';

export class FFMpeg {
  process: ChildProcess;

  constructor(url) {
    const args = ['-rtsp_transport', 'tcp', '-i', url, '-loglevel', 'quiet', '-r', '20', '-codec:a', 'mp2', '-ar', '44100', '-ac', '1', '-b:a', '128k', '-f', 'mpegts', '-b:v', '350k', '-codec:v', 'mpeg1video', '-bf', '0', '-q:a', '0', '-muxdelay', '0.0001', '-'];
    this.process = spawn('ffmpeg', args);
    this.process.stderr.on('data', err => console.log(err));
    this.process.on('close', code => console.log(code));
  }

  subscribe(callback) {
    this.process.stdout.on('data', callback);
  }
}

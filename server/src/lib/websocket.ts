import * as WS from 'ws';
import { Server } from 'http';
import * as uuid from 'uuid';
import { WebSocket, WSClient, WSInitParameters } from '../types/WebSocket';

class WebSocketServer {
  ws: WebSocket;

  constructor({ port }) {
    this.ws = new WS.Server({ port, perMessageDeflate: false });
    this.ws.connectionCount = 0;
  }

  init({ onMessage }: WSInitParameters) {
    const { ws } = this;

    ws.on('connection', (client: WSClient, upgradeReq) => {
      ws.connectionCount += 1;
      client.id = uuid.v4();

      console.log(`Connected: ${upgradeReq.socket.remoteAddress} (Total: ${ws.connectionCount})`);

      client.on('message', (data: string) => {
        let message;

        try {
          message = JSON.parse(data);
          onMessage(client, message);
        } catch (err) {
          console.error('Cannot parse client message..', err);
        }
      });

      client.on('close', (code, message) => {
        ws.connectionCount -= 1;
        console.log(`Disconnected. (Total: ${ws.connectionCount}`)
      })
    })
  }
}

export default WebSocketServer;


import * as path from 'path';
import * as WS from 'ws';

import WebSocketServer from './lib/websocket';
import { MessageTypes, OpenStreamMessage } from './types/WebSocket';
import { FFMpeg } from './lib/ffmpeg';

const ws = new WebSocketServer({ port: process.env.PORT || 8080 });

ws.init({
  onMessage(client, message: OpenStreamMessage) {
    console.log({ clientUUID: client.id, ffmpeg: !!client.ffmpeg });

    if (message.type !== MessageTypes.OPEN_STREAM) {
      return;
    };

    if (!message.url) {
      console.log('No RTSP Url is provided, cannot play!');
      return;
    }

    if (client.ffmpeg) {
      client.ffmpeg.process.kill()
    }


    client.ffmpeg = new FFMpeg(message.url);

    client.ffmpeg.subscribe(data => {
      if (client.readyState === WS.OPEN) {
        client.send(data)
      };
    })
  }
})
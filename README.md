# RTSP Player

Web Player App (includes backend) that allows you to view RTSP streams directly in your browser. Written in Angular.js and Node.js. Uses FFMpeg to transcode RTSP stream into MPEG1 in real time. 

## Prerequisites

What things you need to install the software and how to install them

* Docker

## Installation

A step by step series of examples that tell you have to get a development env running

```bash
git clone https://gitlab.com/bermanboris/rtsp-player.git
cd rtsp-player
docker-compose up
```

## Post Installation
Congratulation! Your app is now running on "http://localhost:8081".